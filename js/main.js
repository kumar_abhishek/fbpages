(function () {
  var accessToken = '533294503491299|8xp7HxdZtrghc9NptlDrunYvDAU';

  function smallInfoUrl(keyword) {
    return 'https://graph.facebook.com/search?access_token=' + accessToken + '&type=page&fields=name,about,description, website,likes,picture&q=' + keyword;
  }

  function pageDetailUrl(page) {
    return 'https://graph.facebook.com/v2.0/' + page + '?access_token=' + accessToken + '&type=page&fields=name,about,description, website,likes,picture,cover,posts';
  }

  function getCorrectUrl(url) {
    var regex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    if (regex.test(url)) {
      if (!url.startsWith('http')) {
        return "http://" + url;
      }
    } else {
      return null;
    }
  }

  function $(selector) {
    return document.querySelector(selector);
  }

  function ajax(method, url, cb) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function (d) {
      if (this.readyState == 4) {
        switch (this.status) {
        case 200:
          var data = null;
          try {
            data = JSON.parse(this.response);
          } catch (e) {}
          cb(data);
          break;
        }
      }
    }
    xhr.open(method, url);
    xhr.send();
  }

  function search() {
    $('body').classList.remove('detail');
    $('body').classList.add('search');
    ajax('GET', smallInfoUrl($('#keyword').value), function (d) {
      var markup = ['<ul>'];
      d.data.forEach(function (item) {
        item.description = item.description || '';
        item.website = getCorrectUrl(item.website);
        markup.push('<li data-id="' + item.id + '">');
        markup.push('<div class="left"><img src="' + item.picture.data.url + '"></img></div>');
        markup.push('<div><a href="#/page/' + item.id + '">' + item.name + '</a></div>');
        markup.push('<div><span class="fs10 colorcyan"> Likes: ' + item.likes + '</span></div>');
        markup.push('<div class="fs12 color_999"> ' + item.about + '</div>' +
          '<div class="inline_block fs12">' + item.description + '</div>');
        if (item.website) {
          markup.push('<div>Website: <a target="_blank" href="' + item.website + '">' + item.website + '</a></div>');
        }
        markup.push('</li>');
      });
      markup.push('</ul>');
      $('#result').innerHTML = markup.join('');
    });
  }

  function pageDetail(page) {
    $('body').classList.remove('search');
    $('body').classList.add('detail');
    ajax('GET', pageDetailUrl(page), function (d) {
      var markup = ['<div id="pagedetail">'];
      d.description = d.description || '';
      d.website = getCorrectUrl(d.website);
      markup.push('<div>' + d.name + '</div>' +
        '<img class="width_full" src="' + d.cover.source + '"></img>' +
        '<div class="fs16 width_full inline_block">' +
        '<span class="left">' + d.about + '</span>' +
        '<span class="right fs12 colorcyan">Likes:' + d.likes + '</span>' +
        '</div>' +
        '<div class="fs14"><br/>Posts:</div>' +
        '<div id="posts" class="fs12">');
      d.posts.data.forEach(function (p) {
        if (p.message) {
          markup.push('<div class="post inline_block width_full">' +
            '<span class="left">' + p.message + '</span>' +
            '<span class="right color_999 fs10">Created: ' + p.created_time +
            '</span></div>');
        }
      });

      markup.push('</div>');
      $('#result').innerHTML = markup.join('');
    });
  }


  function route(e) {
    var page = location.hash.substr(1);
    if (page == '') {
      location.hash = '/';
    } else {
      if (page == '/') {
        $('#keyword').addEventListener('change', search);
        search();
      } else {
        page = page.substr(1).split('/');
        if (page[0] == 'page') {
          pageDetail(page[1]);
        }
      }
    }
  }

  onload = function () {
    console.log('Welcome to Facebook Pages Search WebApp');
    onhashchange = route;
    route();
    //https://graph.facebook.com/search?access_token=533294503491299|8xp7HxdZtrghc9NptlDrunYvDAU&q=sachin&type=page

    //
  };
})();